import http from 'http';
import path from 'path';
const puerto = 3000;
import express from 'express';
import bodyParser from 'body-parser';
import { fileURLToPath } from 'url';

import misRutas from './router/index.js';



const _filename = fileURLToPath(import.meta.url);
const dirname = path.dirname(_filename);

const app = express();

//Asignaciones
app.set("view engine","ejs");

// Configura bodyParser para manejar las solicitudes con cuerpo JSON
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Asignar al objeto información
// Configura los tipos MIME para todos los archivos estáticos
app.use(express.static('public'));





app.use(misRutas.router);

app.listen(puerto, () => {
    console.log("Escuchando servidor");
});
