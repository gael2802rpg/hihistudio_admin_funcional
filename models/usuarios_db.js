import conexion from "./conexion.js";
import { rejects } from "assert";
var usuarios_db = {}

usuarios_db.mostrarTodos = function mostrarTodos() {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "select * from usuarios";
        conexion.query(sqlConsulta, null, function (err, res) {
            
            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

export default usuarios_db;