import conexion from "./conexion.js";
import { rejects } from "assert";
var categorias_db = {}

categorias_db.insertar = function insertar(categoria) {
    return new Promise((resolve, rejects) => {
        // consulta
        let sqlConsulta = "Insert into categorias set ?";
        conexion.query(sqlConsulta, categoria, function (err, res) {

            if (err) {
                console.log("Surgio un error ", err.message);
                rejects(err);
            }
            else {
                const categoria = {
                    id: res.id,
                }
                resolve(categoria);
            }

        });
    });
}

categorias_db.buscar = function buscar(codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "SELECT * FROM categorias WHERE codigo = ? and status = 0";
        conexion.query(sqlConsulta, [codigo], function (err, res) {

            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

categorias_db.modificar = function buscar(categoria, codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE categorias SET ? WHERE codigo = ?";
        conexion.query(sqlConsulta, [categoria, codigo], function (err, res) {
            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

categorias_db.deshabilitar = function buscar(codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE categorias SET status = 1 WHERE codigo = ?";
        conexion.query(sqlConsulta, [codigo], function (err, res) {

            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}


export default categorias_db;