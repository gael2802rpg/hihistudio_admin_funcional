import conexion from "./conexion.js";
import { rejects } from "assert";
var registro_db = {}

registro_db.registrar = function registrar(user) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "Insert into usuarios set ?";
        conexion.query(sqlConsulta, user, function (err, res) {

            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}


export default registro_db;