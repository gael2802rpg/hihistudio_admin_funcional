import conexion from "./conexion.js";
import { rejects } from "assert";
var productos_db = {}

productos_db.insertar = function insertar(producto) {
    return new Promise((resolve, rejects) => {
        // consulta
        let sqlConsulta = "Insert into productos set ?";
        conexion.query(sqlConsulta, producto, function (err, res) {

            if (err) {
                console.log("Surgio un error ", err.message);
                rejects(err);
            }
            else {
                const producto = {
                    id: res.id,
                }
                resolve(producto);
            }

        });
    });
}

productos_db.buscar = function buscar(codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "SELECT * FROM productos WHERE codigo = ? and status = 0";
        conexion.query(sqlConsulta, [codigo], function (err, res) {

            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

productos_db.modificar = function buscar(producto, codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE productos SET ? WHERE codigo = ?";
        conexion.query(sqlConsulta, [producto, codigo], function (err, res) {
            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}

productos_db.deshabilitar = function buscar(codigo) {
    return new Promise((resolve, reject) => {
        var sqlConsulta = "UPDATE productos SET status = 1 WHERE codigo = ?";
        conexion.query(sqlConsulta, [codigo], function (err, res) {

            if (err) {
                console.log("Surgio un error");
                reject(err);
            }
            else {
                resolve(res);
            }
        });
    });
}


export default productos_db;