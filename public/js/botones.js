

document.addEventListener("DOMContentLoaded", function() {
 

    function index(){
        window.location.href = '/'; 
    }

    function login(){
        window.location.href = '/login';
    }

    function registro(){
        window.location.href = '/registro';
    }

    function menu() {
        window.location.href = '/menu';
    
    }

    function productos() {
        window.location.href = '/productos';
    }

    function categorias() {
        window.location.href = '/categorias';
    }

    function usuarios() {
        window.location.href = '/usuarios';
    }

    

    document.getElementById("index").addEventListener("click", index);

    const menuButton = document.getElementById("menu");
    if (menuButton) {
        document.getElementById("menu").addEventListener("click", menu);
    }
    
    const loginButton = document.getElementById("login");
    if (loginButton) {
        loginButton.addEventListener("click", login);
    }

    const registerButton = document.getElementById("registro");
    if (registerButton) {
        registerButton.addEventListener("click", registro);
    }

    document.getElementById("productos").addEventListener("click", productos);
    document.getElementById("categorias").addEventListener("click", categorias);
    document.getElementById("usuarios").addEventListener("click", usuarios); 

    
    
});


