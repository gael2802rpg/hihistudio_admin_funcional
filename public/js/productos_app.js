
document.addEventListener("DOMContentLoaded", function() {
    const btnNuevo = document.getElementById('btnNuevo');
    const btnAgregar = document.getElementById("btnAgregar");
    const btnModificar = document.getElementById("btnModificar");
    const btnDeshabilitar = document.getElementById("btnDeshabilitar");
    const btnLimpiar = document.getElementById('btnLimpiar');
    const btnCancelar = document.getElementById('btnCancelar');
    //const btnBuscar = this.document.getElementById('btnBuscar');

    let codigo, nombre, descripcion, id_categoria, precio, cantidad, imagen, search;
    get_inputs();
    var vali_search = false;
    search = document.getElementById("search");
    search.addEventListener('keyup',async function(e) {
        var keycode = e.keyCode;
        if (keycode === 13) {
            if(!search.value){
                alert('Ingrese un codigo para buscar');
            }
            else{
                var codigo = search.value;

                try {
                    
                    const response = await fetch('/productos_db/buscar', { 
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({ codigo: codigo })
                    });

                    if (response.ok) {
                        const data = await response.json();
                        //console.log(data);
                        if (data) {
                            get_inputs();
                            set_inputs(data);
                            vali_search = true;
                        } else {
                            alert('Producto no encontrado');
                        }
                    } else {
                        
                        console.error('Error al buscar el producto:', response.statusText);
                    }
                } catch (error) {
                    alert('El codigo que está ingresando no existe o está deshabilitado');

                }
            }
            
        }
    });



    btnAgregar.addEventListener("click", async function() {
        try {
            get_inputs();
            if(!valiEmpty){
                alert('Porfavor llene todos los campos antes de agregar');
            }
            else{
                const response = await fetch('/productos_db/insertar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        codigo: codigo.value,
                        nombre: nombre.value,
                        descripcion: descripcion.value,
                        id_categoria: id_categoria.value,
                        precio: precio.value,
                        cantidad: cantidad.value,
                        imagen: imagen.value,
                        ranking: 0,
                        status: 0
                    })
                });
    
                if (response.ok) {
                    alert("Producto agregado exitosamente");
                    limpiar();
                    
                } else {
                    alert("Error al agregar producto");
                }
                
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud:", error);
        }
        
    });

    btnModificar.addEventListener('click',async function(){
        try {
            if(!vali_search){
                alert('Busque un producto para modificar');
            }
            else{
                get_inputs();
            
                const response = await fetch('/productos_db/modificar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        nombre: nombre.value,
                        descripcion: descripcion.value,
                        id_categoria: id_categoria.value,
                        precio: precio.value,
                        cantidad: cantidad.value,
                        imagen: imagen.value,
                        codigo: codigo.value
                        
                    })
                });
                
                if (response.ok) {
                    alert("Producto modificado exitosamente");
                    
                    limpiar();
                    
                } else {
                    alert("Error al modificar producto");
                }
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud: " + error.message);
        }
        
    });
    
    btnDeshabilitar.addEventListener('click',async function(){
        try {
            if(!vali_search){
                alert('Busque un producto para deshabilitar');
            }
            else{
                get_inputs();
                const response = await fetch('/productos_db/deshabilitar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        codigo: codigo.value
                    })
                });
                
                if (response.ok) {
                    alert("Producto deshabilitado exitosamente");
                    limpiar();
                    
                } else {
                    alert("Error al deshabilitar producto");
                }
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud: " + error.message);
        }
        
    });

    btnLimpiar.addEventListener('click',function(){
        limpiar();
    });


    function get_inputs(){
        codigo = document.getElementById("codigo");
        nombre = document.getElementById("nombre");
        descripcion = document.getElementById("descripcion");
        id_categoria = document.getElementById("id_categoria");
        precio = document.getElementById("precio");
        cantidad = document.getElementById("cantidad");
        imagen = document.getElementById("url"); 
    }

    function set_inputs(data){
  
        codigo.value = data[0].codigo;
        nombre.value = data[0].nombre;
        descripcion.value = data[0].descripcion;
        id_categoria.value = data[0].id_categoria;
        precio.value = data[0].precio;
        cantidad.value = data[0].cantidad;
        imagen.value = data[0].imagen;
    }

    function limpiar(){
        codigo.value = '';
        nombre.value = '';
        descripcion.value = '';
        id_categoria.value = '';
        precio.value = '';
        cantidad.value = '';
        imagen.value = '';
        search.value = '';
        vali_search = false;
    }

    function valiEmpty(){
        console.log()
        if(!codigo.value && 
            !nombre.value && 
            !descripcion.value && 
            !id_categoria.value && 
            !precio.value && 
            !cantidad.value && 
            !imagen.value){
            return false;
        }
        else{
            return true
        }
    }
});

