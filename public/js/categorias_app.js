document.addEventListener("DOMContentLoaded", function() {
    const btnNuevo = document.getElementById('btnNuevo');
    const btnAgregar = document.getElementById("btnAgregar");
    const btnModificar = document.getElementById("btnModificar");
    const btnDeshabilitar = document.getElementById("btnDeshabilitar");
    const btnLimpiar = document.getElementById('btnLimpiar');
    const btnCancelar = document.getElementById('btnCancelar');
    //const btnBuscar = this.document.getElementById('btnBuscar');

    let codigo, tipo, genero, search;
    get_inputs();
    //habilitar(false);
    var vali_search = false;
    search = document.getElementById("search");
    search.addEventListener('keyup',async function(e) {
        var keycode = e.keyCode;
        if (keycode === 13) {
            if(!search.value){
                alert('Ingrese un codigo para buscar');
            }
            else{
                var codigo = search.value;

                try {
                    
                    const response = await fetch('/categorias_db/buscar', { 
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({ codigo: codigo })
                    });

                    if (response.ok) {
                        const data = await response.json();
                        //console.log(data);
                        if (data) {
                            get_inputs();
                            set_inputs(data);
                            vali_search = true;
                        } else {
                            alert('Categoria no encontrada');
                        }
                    } else {
                        
                        console.error('Error al buscar la categoria:', response.statusText);
                    }
                } catch (error) {
                    alert('El codigo que está ingresando no existe o está deshabilitado');

                }
            }
            
        }
    });

    /*
    btnNuevo.addEventListener('click',function(){
        get_inputs();
        habilitar(false);
    });*/

    btnAgregar.addEventListener("click", async function() {
        try {
            get_inputs();
            if(!valiEmpty){
                alert('Porfavor llene todos los campos antes de agregar');
            }
            else{
                const response = await fetch('/categorias_db/insertar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        codigo: codigo.value,
                        tipo: tipo.value,
                        genero: genero.value,
                        status: 0
                    })
                });
    
                if (response.ok) {
                    alert("Categoria agregado exitosamente");
                    limpiar();
                    
                } else {
                    alert("Error al agregar categoria");
                }
                
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud:", error);
        }
        
    });

    btnModificar.addEventListener('click',async function(){
        try {
            if(!vali_search){
                alert('Busque un producto para modificar');
            }
            else{
                get_inputs();
            
                const response = await fetch('/categorias_db/modificar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        tipo: tipo.value,
                        genero: genero.value,
                        codigo: codigo.value
                        
                    })
                });
                
                if (response.ok) {
                    alert("Categoria modificada exitosamente");
                    
                    limpiar();
                    
                } else {
                    alert("Error al modificar categoria");
                }
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud: " + error.message);
        }
        
    });
    
    btnDeshabilitar.addEventListener('click',async function(){
        try {
            if(!vali_search){
                alert('Busque una categoria para deshabilitar');
            }
            else{
                get_inputs();
                const response = await fetch('/categorias_db/deshabilitar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        codigo: codigo.value
                    })
                });
                
                if (response.ok) {
                    alert("Categoria deshabilitada exitosamente");
                    limpiar();
                    
                } else {
                    alert("Error al deshabilitar Categoria");
                }
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud: " + error.message);
        }
        
    });

    btnLimpiar.addEventListener('click',function(){
        limpiar();
    });

    /*
    btnCancelar.addEventListener('click',function(){
        inputs();
        habilitar(true);
        limpiar();
        vali_search = false;
    });*/

    function get_inputs(){
        codigo = document.getElementById("codigo");
        tipo = document.getElementById("tipo");
        genero = document.getElementById("genero");  
    }

    function set_inputs(data){
        codigo.value = data[0].codigo;
        tipo.value = data[0].tipo;
        genero.value = data[0].genero;
    }

    /*
    function habilitar(vali){
        codigo.disabled = vali;
        tipo.disabled = vali;
        genero.disabled = vali;

        btnAgregar.disabled = vali;
        btnModificar.disabled = vali;
        btnDeshabilitar.disabled = vali;
        btnLimpiar.disabled = vali;
        //btnCancelar.disabled = vali;
    }
    */

    function limpiar(){
        codigo.value = '';
        tipo.value = '';
        genero.value = '';

        vali_search = false;
    }

    function valiEmpty(){
        console.log()
        if(!codigo.value && !tipo.value && !genero.value){
            return false;
        }
        else{
            return true
        }
    }
});