document.addEventListener("DOMContentLoaded", function() {
    const btnRegistrar = document.getElementById('btnRegistrar');

    let firstName, lastName, username, birthdate, email, password;
    get_inputs();
  
    btnRegistrar.addEventListener("click", async function() {
        try {
            get_inputs();
            if(!valiEmpty()){
                alert('Porfavor llene todos los campos antes de registrar');
            }
            else{
                const response = await fetch('/registro_db/registrar', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName.value,
                        lastName: lastName.value,
                        username: username.value,
                        birthdate: birthdate.value,
                        email: email.value,
                        password: password.value,
                    })
                });
    
                if (response.ok) {
                    alert("Usuario agregado exitosamente");
                    limpiar();
                    
                } else {
                    alert("Error al registrar usuario");
                }
                
            }
            
        } catch (error) {
            alert("Error al procesar la solicitud:", error);
        }
        
    });

    function get_inputs(){
        firstName = document.getElementById("firstName");
        lastName = document.getElementById("lastName");
        username = document.getElementById("username");  
        birthdate = document.getElementById("birthdate");  
        email = document.getElementById("email");  
        password = document.getElementById("password");  
    }

    function valiEmpty(){
        console.log(birthdate.value);
        if(!firstName.value && 
            !lastName.value && 
            !username.value && 
            !birthdate.value && 
            !email.value &&
            !password.value){
            return false;
        }
        else{
            return true
        }
    }
});