

let articule;
async function mostrar() {
    try {
        const response = await fetch('/main_db/mostrarTodos');
        if (response.ok) {
            const data = await response.json();
            console.log(data[0]);
            mostrar_cartas(data);
            if (data) {
               
                set_inputs(data);  
            } else {
                alert('Producto no encontrado');
            }
        } else {

            console.error('Error al buscar el producto:', response.statusText);
        }
    } catch (error) {
        alert('Hubo un problema en procesar la información del producto' + error.message);

    }
}

function mostrar_cartas(data){
    longitud = data.length;
    console.log(longitud);
    cant_product = (longitud/3);
    for(let x = cant_product; x > 0; x--){
        articulos(cant_product);
        cant_product--;
    }
}

function articulos(cant){
    const section = document.getElementById('cuerpo');
    articule = document.createElement('article');
    
   
    if(cant <= 0.4){
        cards(section);
    }
    else if(cant >=0.5 && cant <=0.7){
        for(let x=0;x<2;x++){
            cards(section);
        }
    }
    else{
        for(let x=0;x<3;x++){
            cards(section);
        }
    }
    
  
}

function cards(section){

    // Creación de Elementos
    const card = document.createElement('div');
    const img1 = document.createElement('img');

    const div1 = document.createElement('div');

    const h3_1 = document.createElement('h3');

    const p_1 = document.createElement('p');
    const p_2 = document.createElement('p');

    
    card.classList.add('product-item');
    // Asignación de clases
    img1.classList.add('product-image');
    div1.classList.add('product-info');
    h3_1.classList.add('product-title');
    p_1.classList.add('product-description');
    p_2.classList.add('product-price');

    section.appendChild(articule);
    articule.appendChild(card);
    card.appendChild(img1);
    card.appendChild(div1);
    div1.appendChild(h3_1);
    div1.appendChild(p_1);
    div1.appendChild(p_2);
}

function set_inputs(data){
    for(let y=0;y<data.length;y++){
        console.log(data[y]);
        //-----------Agregando Imagenes-----------
        var cards = document.querySelectorAll('.product-item');
        var imgElement = cards[y].querySelector('.product-image');
        imgElement.src = data[y].imagen;
        //-----------Agregando Titulo-----------
        var cardInfo = document.querySelectorAll('.product-info');
        var title = cardInfo[y].querySelector('.product-title');
        title.textContent = data[y].nombre;
        //-----------Agregando Descriçión-----------
        var descripcion = cardInfo[y].querySelector('.product-description');
        descripcion.textContent = data[y].descripcion;
        //-----------Agregando Precio-----------
        var precio = cardInfo[y].querySelector('.product-price');
        precio.textContent = "$" + data[y].precio;
    }
}

mostrar();