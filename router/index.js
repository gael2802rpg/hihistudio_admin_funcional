import express from "express";
import json from 'body-parser';
export const router = express.Router();
import productos_db from "../models/productos_db.js";
import categorias_db from "../models/categorias_db.js";
import usuarios_db from "../models/usuarios_db.js";
import login_db from "../models/login_db.js";
import registro_db from "../models/registro_db.js";
import main_db from "../models/main_db.js";



router.get('/', async (req, res) => {
    
    res.render('index', { reg: rows });
    
});

router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/menu', (req, res) => {
    res.render('menu');
});

let rows;
router.get('/productos', async (req, res) => {
    rows = await productos_db.buscar();
    res.render('productos', { reg: rows });
})

let params;
router.post('/productos_db/insertar', async (req, res) => {
    try {
       
        params = {
            codigo: req.body.codigo,
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            id_categoria: req.body.id_categoria,
            precio: req.body.precio,
            cantidad: req.body.cantidad,
            imagen: req.body.imagen,
            ranking: req.body.ranking,
            status: req.body.status

        }
        const registros = await productos_db.insertar(params);
        console.log("-------------- registros " + registros);
        
       

    } catch (error) {
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    res.render('productos', { reg: rows });
    
});

let params2;
router.post('/productos_db/modificar', async (req, res) => {
    try {
        var codigo = req.body.codigo;
        params2 = {
            nombre: req.body.nombre,
            descripcion: req.body.descripcion,
            id_categoria: req.body.id_categoria,
            precio: req.body.precio,
            cantidad: req.body.cantidad,
            imagen: req.body.imagen,
            ranking: req.body.ranking,
            status: req.body.status
        }
        const modificaciones = await productos_db.modificar(params2, codigo);
        console.log("-------------- Modificaciones " + modificaciones);
        res.status(200).send("Producto modificado exitosamente");
    } catch (error) {
        console.error(error)
        res.status(500).send("sucedio un error: " + error);
    }
});


router.post('/productos_db/deshabilitar', async (req, res) => {
    try {
        var codigo = req.body.codigo;
        const modificaciones = await productos_db.deshabilitar(codigo);
        console.log("-------------- Modificaciones " + modificaciones);
        res.status(200).send("Producto modificado exitosamente");
    } catch (error) {
        console.error(error)
        res.status(500).send("sucedio un error: " + error);
    }
});

router.post('/productos_db/buscar', async (req, res) => {
    try {
        const codigo = req.body.codigo;
        const producto = await productos_db.buscar(codigo);
        //res.render('productos', { reg: producto });
        res.json(producto);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error al buscar el producto");
    }
    
});

router.get('/categorias',async (req, res) => {
    rows = await categorias_db.buscar();
    res.render('categorias');
});


router.post('/categorias_db/insertar', async (req, res) => {
    try {
       
        params = {
            codigo: req.body.codigo,
            tipo: req.body.tipo,
            genero: req.body.genero,
            status: req.body.status

        }
        const registros = await categorias_db.insertar(params);
        console.log("-------------- registros " + registros);
        
       

    } catch (error) {
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    res.render('productos', { reg: rows });
    
});


router.post('/categorias_db/modificar', async (req, res) => {
    try {
        var codigo = req.body.codigo;
        params2 = {
            tipo: req.body.tipo,
            genero: req.body.genero,
            status: req.body.status
        }
        const modificaciones = await categorias_db.modificar(params2, codigo);
        console.log("-------------- Modificaciones " + modificaciones);
        res.status(200).send("Categoria modificada exitosamente");
    } catch (error) {
        console.error(error)
        res.status(500).send("sucedio un error: " + error);
    }
});


router.post('/categorias_db/deshabilitar', async (req, res) => {
    try {
        var codigo = req.body.codigo;
        const modificaciones = await categorias_db.deshabilitar(codigo);
        console.log("-------------- Modificaciones " + modificaciones);
        res.status(200).send("Producto modificado exitosamente");
    } catch (error) {
        console.error(error)
        res.status(500).send("sucedio un error: " + error);
    }
});

router.post('/categorias_db/buscar', async (req, res) => {
    try {
        const codigo = req.body.codigo;
        const categorias = await categorias_db.buscar(codigo);
        res.json(categorias);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error al buscar el producto");
    }
    
});

router.get('/usuarios', async (req, res) => {
    rows = await usuarios_db.mostrarTodos();
    res.render('usuarios', { reg: rows });

});

router.get('/login',async (req, res) => {
    res.render('login');
});

router.post('/login_db/iniciar', async (req, res) => {
    try {
        const username = req.body.username;
        const password = req.body.password;
        const login = await login_db.iniciar(username,password);
        res.json(login);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error al iniciar seción");
    }
    
});

router.get('/registro',async (req, res) => {
    res.render('registro');
});

router.post('/registro_db/registrar', async (req, res) => {
    try {
       
        params = {
            card: 1,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            username: req.body.username,
            birthdate: req.body.birthdate,
            email: req.body.email,
            password: req.body.password,
            status: 0

        }
        const registros = await registro_db.registrar(params);
        console.log("-------------- registros " + registros);
        
    } catch (error) {
        console.error(error)
        res.status(400).send("sucedio un error: " + error);
    }
    
    
});


router.get('/main_db/mostrarTodos', async (req, res) => {
    try {
        const main = await main_db.mostrarTodos();
        res.json(main);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error al mostrar productos");
    }
    
});

/*async function prueba() {

    try {

        const res = await conexion.execute("select * from alumnos");
        console.log("El resultado es ", res);

    } catch (error) {

        console.log("Surgio un error ", error);

    } finally {

    }

}*/

export default { router };
