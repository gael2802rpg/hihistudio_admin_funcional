/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.4.28-MariaDB-log : Database - hihistudio_db
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`hihistudio_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;

USE `hihistudio_db`;

/*Table structure for table `categorias` */

DROP TABLE IF EXISTS `categorias`;

CREATE TABLE `categorias` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `tipo` varchar(100) DEFAULT NULL,
  `genero` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `categorias` */

insert  into `categorias`(`id_categoria`,`codigo`,`tipo`,`genero`,`status`) values (1,'C-01','pin_small','anime',0),(2,'C-02','pin_mediano','anime',0),(3,'C-03','pin_grande','anime',0),(4,'C-04','stickers','anime',0),(5,'C-05','paquete_stickers','anime',0),(6,'C-06','plantilla_stickers','anime',0),(7,'C-07','camisas','anime',0),(8,'C-08','llaveros','anime',0),(9,'C-09','tazas','peliculas',0),(10,'C-10','libretas','libros',0),(11,'C-11','Libretas medianas','videojuegos',0);

/*Table structure for table `compras` */

DROP TABLE IF EXISTS `compras`;

CREATE TABLE `compras` (
  `id_compras` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  `proveedor` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_compras`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `compras` */

insert  into `compras`(`id_compras`,`codigo`,`fecha`,`total`,`proveedor`) values (1,'C-001','2024-01-01',500,'SamsClub'),(2,'C-002','2024-02-15',750,'Amazon'),(3,'C-003','2024-03-10',300,'Walmart'),(4,'C-004','2024-04-05',1000,'BestBuy'),(5,'C-005','2024-05-20',600,'Target');

/*Table structure for table `detalles_compras` */

DROP TABLE IF EXISTS `detalles_compras`;

CREATE TABLE `detalles_compras` (
  `id_detalles_compra` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `id_compras` int(11) DEFAULT NULL,
  `material` varchar(100) DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `precio_unitario` float DEFAULT NULL,
  PRIMARY KEY (`id_detalles_compra`),
  KEY `id_compras` (`id_compras`),
  CONSTRAINT `detalles_compras_ibfk_1` FOREIGN KEY (`id_compras`) REFERENCES `compras` (`id_compras`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detalles_compras` */

insert  into `detalles_compras`(`id_detalles_compra`,`codigo`,`id_compras`,`material`,`cantidad`,`precio_unitario`) values (1,'DC-001',1,'papel tamaño carta',2,250),(2,'DC-002',2,'acero inoxidable',5,100),(3,'DC-003',3,'plástico resistente',10,30),(4,'DC-004',4,'madera de roble',3,150),(5,'DC-005',5,'vidrio templado',8,50);

/*Table structure for table `detalles_ventas` */

DROP TABLE IF EXISTS `detalles_ventas`;

CREATE TABLE `detalles_ventas` (
  `id_detalles_ventas` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `id_producto` int(11) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `precio_unitario` float DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_detalles_ventas`),
  KEY `id_producto` (`id_producto`),
  CONSTRAINT `detalles_ventas_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id_producto`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `detalles_ventas` */

insert  into `detalles_ventas`(`id_detalles_ventas`,`codigo`,`id_producto`,`descripcion`,`precio_unitario`,`cantidad`) values (1,'DV-001',1,'venta de pin Jotaro',10,1),(2,'DV-002',2,'venta de stickers One Piece',5,2),(3,'DV-003',3,'venta de camisa Dragon Ball',20,1),(4,'DV-004',4,'venta de llavero Naruto',8,3),(5,'DV-005',5,'venta de taza Studio Ghibli',15,2);

/*Table structure for table `metodo_pago` */

DROP TABLE IF EXISTS `metodo_pago`;

CREATE TABLE `metodo_pago` (
  `id_metodo_pago` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `tipo_pago` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_metodo_pago`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `metodo_pago` */

insert  into `metodo_pago`(`id_metodo_pago`,`codigo`,`tipo_pago`) values (1,'MP-001','Visa'),(2,'MP-002','MasterCard'),(3,'MP-003','American Express'),(4,'MP-004','PayPal'),(5,'MP-005','Bitcoin');

/*Table structure for table `productos` */

DROP TABLE IF EXISTS `productos`;

CREATE TABLE `productos` (
  `id_producto` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  `id_categoria` int(11) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `cantidad` int(11) DEFAULT NULL,
  `imagen` varchar(500) DEFAULT NULL,
  `ranking` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_producto`),
  KEY `id_categoria` (`id_categoria`),
  CONSTRAINT `productos_ibfk_2` FOREIGN KEY (`id_categoria`) REFERENCES `categorias` (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `productos` */

insert  into `productos`(`id_producto`,`codigo`,`nombre`,`descripcion`,`id_categoria`,`precio`,`cantidad`,`imagen`,`ranking`,`status`) values (1,'P-0001','Acuario','Este paquete de stickers es perfecto para cualquier persona que le guste la vida marina y colores az',1,10,10,'https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F1.png?alt=media&token=49c77236-7a71-42be-aa32-313037004301',0,0),(2,'P-0002','Samrio','Paquete de stickers con tematica Sanrio, para llenar tu libreta con tus personajes favoritos!!!',4,5,20,'https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F36.png?alt=media&token=c29e4212-7e79-4102-9779-3b0cda157413',0,0),(3,'P-0003','Jungkook','Paquete de stickers de BTS Jungkook color marron, para que combine con tus accesorios preferidos',7,20,5,'https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F9.png?alt=media&token=91f0f404-7cf1-4fe7-a8ed-0b0bb7b51170',0,0),(4,'P-0004','Jin','Paquete de stickers de BTS Jin color rosa, para justos coloridos',8,8,15,'https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F8.png?alt=media&token=f9ef0784-e3ae-4234-bb25-e52146581546',0,0),(5,'P-0005','Hobby','Paquete de stickers de BTS Hobby color arcoiris, combina con todos tus gustos',9,15,8,'https://firebasestorage.googleapis.com/v0/b/proyectoweb-f2ea4.appspot.com/o/img%2F6.png?alt=media&token=b6e93147-052f-48d9-8131-be5d98cb6342',0,0);

/*Table structure for table `tarjetas` */

DROP TABLE IF EXISTS `tarjetas`;

CREATE TABLE `tarjetas` (
  `id_tarjetas` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `tipo_tarjeta` varchar(100) DEFAULT NULL,
  `num_tarjeta` varchar(16) DEFAULT NULL,
  `fecha_vencimiento` varchar(12) DEFAULT NULL,
  `nom_titular` varchar(100) DEFAULT NULL,
  `direc_facturacion` varchar(100) DEFAULT NULL,
  `cod_postal` varchar(8) DEFAULT NULL,
  `cvv` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`id_tarjetas`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `tarjetas` */

insert  into `tarjetas`(`id_tarjetas`,`codigo`,`tipo_tarjeta`,`num_tarjeta`,`fecha_vencimiento`,`nom_titular`,`direc_facturacion`,`cod_postal`,`cvv`) values (1,'T-001','Aux','aux','aux','aux','aux','aux','aux'),(2,'T-002','Visa','4815163051309295','04/28','José Enrique Santos Osuna','Mazatlán, Sinaloa, Mexico','18921','676'),(3,'T-003','MasterCard','5421888417448410','06/25','María Fernanda García López','Guadalajara, Jalisco, Mexico','44128','754'),(4,'T-004','American Express','341962784158238','09/27','Juan Carlos Martínez Pérez','Monterrey, Nuevo León, Mexico','64871','937'),(5,'T-005','Discover','6011943542874445','12/23','Ana Karen González Gómez','Puebla, Puebla, Mexico','72450','543'),(6,'T-006','Diners Club','30196714994384','03/26','Pedro Luis Hernández Ruiz','Tijuana, Baja California, Mexico','22150','821');

/*Table structure for table `usuarios` */

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `card` int(11) DEFAULT NULL,
  `firstName` varchar(100) DEFAULT NULL,
  `lastName` varchar(100) DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `card` (`card`),
  CONSTRAINT `card` FOREIGN KEY (`card`) REFERENCES `tarjetas` (`id_tarjetas`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `usuarios` */

insert  into `usuarios`(`id_usuario`,`card`,`firstName`,`lastName`,`username`,`birthdate`,`email`,`password`,`status`) values (1,2,'Arturo Alexis','Santos Osuna','ArturoSantos','2003-09-18','Arturo.Santos@gmail.com','Arturo123',0),(2,3,'María Fernanda','García López','MariaGarcia','1995-05-10','Mafe_Garcia@hotmail.com','Mafe456',0),(3,4,'Juan Carlos','Martínez Pérez','JuanMartinez','1988-12-03','JCMartinez@gmail.com','Juan123',0),(4,5,'Pedro Luis','Hernández Ruiz','PedroHernandez','1999-03-15','PedritoHR@gmail.com','Pedro789',0),(5,6,'Ana Gonzalez','González Gómez','AnaGonzalez','2000-07-25','Anita_Karen@yahoo.com','Ana456',0),(9,1,'Gael Mizraim','Salas Salazar','Gael2802','2002-05-28','gael.salas@hotmail.com','Gael123',1);

/*Table structure for table `ventas` */

DROP TABLE IF EXISTS `ventas`;

CREATE TABLE `ventas` (
  `id_venta` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` varchar(100) DEFAULT NULL,
  `id_detalles_ventas` int(11) DEFAULT NULL,
  `id_usuarios` int(11) DEFAULT NULL,
  `id_metodo_pago` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `total` float DEFAULT NULL,
  PRIMARY KEY (`id_venta`),
  KEY `id_usuarios` (`id_usuarios`),
  KEY `id_metodo_pago` (`id_metodo_pago`),
  KEY `id_detalles_ventas` (`id_detalles_ventas`),
  CONSTRAINT `ventas_ibfk_1` FOREIGN KEY (`id_usuarios`) REFERENCES `usuarios` (`id_usuario`),
  CONSTRAINT `ventas_ibfk_2` FOREIGN KEY (`id_metodo_pago`) REFERENCES `metodo_pago` (`id_metodo_pago`),
  CONSTRAINT `ventas_ibfk_3` FOREIGN KEY (`id_detalles_ventas`) REFERENCES `detalles_ventas` (`id_detalles_ventas`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

/*Data for the table `ventas` */

insert  into `ventas`(`id_venta`,`codigo`,`id_detalles_ventas`,`id_usuarios`,`id_metodo_pago`,`fecha`,`total`) values (1,'V-001',1,1,1,'2024-01-16',10),(2,'V-002',2,2,2,'2024-02-20',15),(3,'V-003',3,3,3,'2024-03-25',20),(4,'V-004',4,4,4,'2024-04-30',25),(5,'V-005',5,5,5,'2024-05-05',30);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
